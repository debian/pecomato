DEL_DIR = rmdir /Q /S
DEL_FILE = del /Q /F
COPY_FILE = copy /Y
CREATE_DIR = mkdir
CHECK_EXIST_DIR = if exist
CHECK_NOT_EXIST_DIR = if not exist
CHECK_EXIST_FILE = if exist
CHECK_NOT_EXIST_FILE = if not exist
RENAME_FILE = move /Y

PYTHON = python
PERL = perl
ZIP = zip -9r -S
RAR = rar a -ow -mdg -m5 -s -r -cfg- -y
RARSFX = $(RAR) -sfx
7ZIP = 7z a -bd -y

include Makefile.inc

SYSTBASE = win32
SYST = $(SYSTBASE)-%PROCESSOR_ARCHITECTURE%

SOURCES = *.c *.h
MKFILES = Makefile*.inc Makefile.darwin Makefile.beos Makefile.linux Makefile.unix Makefile.qnx Makefile.win32
RESOURCES = *.ico *.bin *.in
DOC_SOURCES = doc\*.t2t
DOC_MKFILES = doc\Makefile.posix doc\Makefile.win32 doc\Makefile*.inc
DOC_RESOURCES = doc\*.rc doc\*.cfg
TOOLS = devtools\*.py devtools\*.pl devtools\*.sh

CVS2CL = devtools\cvs2cl.pl -bPS --gmt --fsf --FSF
