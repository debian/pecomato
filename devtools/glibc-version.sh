#!/bin/sh

ldd --version | head -1 | sed "s/[^0-9\.]/ /g" | tr -d ' '
exit $?
