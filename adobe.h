#ifndef _adobe_h_
#define _adobe_h_


#include "util.h"


#define ADOBE_8BPS_MARKER	0x38425053UL	/* 8BPS from Photoshop 3.0 */
#define ADOBE_8BIM_MARKER	0x3842494dUL	/* 8BIM from Adobe IRB */
#define ADOBE_FFO_MARKER    0xaa			/* FFO from Photoshop 6.0+ */

/* Photoshop fields (specs from 2.0 up to 6.0) */
enum ADOBE_RESOURCE
{
	ADOBE_RSC_RASTER_INFO 							=0x03e8,
	ADOBE_RSC_MACINTOSH_PRINT_INFO					=0x03e9,
	ADOBE_RSC_INDEXED_COLOR_TABLE 					=0x03eb,
	ADOBE_RSC_RESOLUTION  							=0x03ed,
	ADOBE_RSC_ALPHA_CHANNEL_NAMES 					=0x03ee,
	ADOBE_RSC_DISPLAY 								=0x03ef,
	ADOBE_RSC_CAPTION 								=0x03f0,
	ADOBE_RSC_BORDER_STYLE							=0x03f1,
	ADOBE_RSC_BACRKGROUND_COLOR						=0x03f2,
	ADOBE_RSC_PRINT_FLAGS 							=0x03f3,
	ADOBE_RSC_GRAYSCALE_MULTICHANNEL_HALFTONING		=0x03f4,
	ADOBE_RSC_COLOR_HALFTONING						=0x03f5,
	ADOBE_RSC_DUOTONE_HALFTONING  					=0x03f6,
	ADOBE_RSC_GRAYSCALE_MULTICHANNEL_TRANSFER 		=0x03f7,
	ADOBE_RSC_COLOR_TRANSFER  						=0x03f8,
	ADOBE_RSC_DUOTONE_TRANSFER						=0x03f9,
	ADOBE_RSC_DUOTONE_IMAGE							=0x03fa,
	ADOBE_RSC_BW_VALUES								=0x03fb,
	ADOBE_RSC_OBSOLETE1								=0x03fc,
	ADOBE_RSC_EPS_OPTIONS 							=0x03fd,
	ADOBE_RSC_QUICK_MASK  							=0x03fe,
	ADOBE_RSC_OBSOLETE2								=0x03ff,
	ADOBE_RSC_LAYER_STATE 							=0x0400,
	ADOBE_RSC_WORKING_PATH							=0x0401,
	ADOBE_RSC_LAYER_GROUP 							=0x0402,
	ADOBE_RSC_OBSOLETE3								=0x0403,
	ADOBE_RSC_IPTC_FILE_INFO						=0x0404,
	ADOBE_RSC_IMAGE_MODE  							=0x0405,
	ADOBE_RSC_JPEG_QUALITY							=0x0406,
	ADOBE_RSC_GRID_GUIDES_INFO						=0x0408,
	ADOBE_RSC_THUMBNAIL_RESOURCE1 					=0x0409,
	ADOBE_RSC_COPYRIGHT_FLAG  						=0x040a,
	ADOBE_RSC_UNIFORM_RESOURCE_LOCATOR				=0x040b,
	ADOBE_RSC_THUMBNAIL_RESOURCE2 					=0x040c,
	ADOBE_RSC_GLOBAL_ANGLE							=0x040d,
	ADOBE_RSC_COLOR_SAMPLERS_RESOURCE1				=0x040e,
	ADOBE_RSC_ICC_PROFILE 							=0x040f,
	ADOBE_RSC_WATERMARK								=0x0410,
	ADOBE_RSC_ICC_UNTAGGER							=0x0411,
	ADOBE_RSC_EFFECTS_VISIBLE 						=0x0412,
	ADOBE_RSC_SPOT_HALFTONE							=0x0413,
	ADOBE_RSC_DOCUMENT_SPECIFIC_IDS					=0x0414,
	ADOBE_RSC_UNICODE_ALPHA_NAMES 					=0x0415,
	ADOBE_RSC_INDEXED_COLOR_TABLE_COUNT				=0x0416,
	ADOBE_RSC_TRANSPARENT_INDEX						=0x0417,
	ADOBE_RSC_GLOBAL_ALTITUDE 						=0x0419,
	ADOBE_RSC_SLICES  								=0x041a,
	ADOBE_RSC_WORKFLOW_URL							=0x041b,
	ADOBE_RSC_XPEP_JUMP								=0x041c,
	ADOBE_RSC_ALPHA_IDENTIFIERS						=0x041d,
	ADOBE_RSC_URL_LIST								=0x041e,
	ADOBE_RSC_VERSION_INFO							=0x0421,
	ADOBE_RSC_CLIPPING_PATH							=0x0bb7,
	ADOBE_RSC_PRINT_FLAGS_INFO						=0x2710,
	/* reserved */
	ADOBE_RSC_EOT									=0xffff
};

struct adobe_resource_descr
{
	unsigned short int resource;
	char* label;
};

struct adobe_resource_range_descr
{
	unsigned short int lower_boundary;
	unsigned short int upper_boundary;
	char* label;
};


void adobe_8bps_parse(unsigned char*, unsigned long int, struct parser_result*);
void adobe_ffo_parse(unsigned char*, unsigned long int, const unsigned short int, struct parser_result*);
void adobe_8bim_parse(unsigned char*, unsigned long int, const unsigned short int, struct parser_result*);
struct adobe_resource_descr* adobe_match_resource(const enum ADOBE_RESOURCE);
struct adobe_resource_range_descr* adobe_match_resource_range(const enum ADOBE_RESOURCE);
void adobe_dump_resource_list(void);

#endif
