#ifndef _jfif_h_
#define _jfif_h_


#include "util.h"


#define JFIF		0x4a464946UL	/* JFIF (followed by 0x00) */
#define JFXX		0x4a465858UL	/* JFXX (followed by 0x00) */
#define JFIF_TAG	0xff	/* preceeds all JFIF markers */


enum JFIF_MARKER
{
	/* frame markers, non-differential, huffman coding */
	JFIF_SOF0	=0xc0,
	JFIF_SOF1	=0xc1,
	JFIF_SOF2	=0xc2,
	JFIF_SOF3	=0xc3,
	/* frame markers, differential, huffman coding */
	JFIF_SOF5	=0xc5,
	JFIF_SOF6	=0xc6,
	JFIF_SOF7	=0xc7,
	/* frame markers, non-differential, arithmetic coding */
	JFIF_JPG	=0xc8,
	JFIF_SOF9	=0xc9,
	JFIF_SOF10	=0xca,
	JFIF_SOF11	=0xcb,
	/* frame markers, differential, arithmetic coding */
	JFIF_SOF13	=0xcd,
	JFIF_SOF14	=0xce,
	JFIF_SOF15	=0xcf,
	/* huffman table specification */
	JFIF_DHT	=0xc4,
	/* arithmetic coding conditioning specification */
	JFIF_DAC	=0xcc,
	/* restart internal termination */
	JFIF_RST0	=0xd0,
	JFIF_RST1	=0xd1,
	JFIF_RST2	=0xd2,
	JFIF_RST3	=0xd3,
	JFIF_RST4	=0xd4,
	JFIF_RST5	=0xd5,
	JFIF_RST6	=0xd6,
	JFIF_RST7	=0xd7,
	/* other markers */
	JFIF_SOI	=0xd8,
	JFIF_EOI	=0xd9,
	JFIF_SOS	=0xda,
	JFIF_DQT	=0xdb,
	JFIF_DNL	=0xdc,
	JFIF_DRI	=0xdd,
	JFIF_DHP	=0xde,
	JFIF_EXP	=0xdf,
	JFIF_APP0	=0xe0,
	JFIF_APP1	=0xe1,
	JFIF_APP2	=0xe2,
	JFIF_APP3	=0xe3,
	JFIF_APP4	=0xe4,
	JFIF_APP5	=0xe5,
	JFIF_APP6	=0xe6,
	JFIF_APP7	=0xe7,
	JFIF_APP8	=0xe8,
	JFIF_APP9	=0xe9,
	JFIF_APP10	=0xea,
	JFIF_APP11	=0xeb,
	JFIF_APP12	=0xec,
	JFIF_APP13	=0xed,
	JFIF_APP14	=0xee,
	JFIF_APP15	=0xef,
	JFIF_JPG0	=0xf0,
	JFIF_JPG1	=0xf1,
	JFIF_JPG2	=0xf2,
	JFIF_JPG3	=0xf3,
	JFIF_JPG4	=0xf4,
	JFIF_JPG5	=0xf5,
	JFIF_JPG6	=0xf6,
	JFIF_JPG7	=0xf7,
	JFIF_JPG8	=0xf8,
	JFIF_JPG9	=0xf9,
	JFIF_JPG10	=0xfa,
	JFIF_JPG11	=0xfb,
	JFIF_JPG12	=0xfc,
	JFIF_JPG13	=0xfd,
	JFIF_COM	=0xfe,
	/* reserved markers */
	JFIF_TEM	=0x01,
	/* reserved */
	JFIF_MARKER_EOT	=0
};

struct jfif_marker_descr
{
	unsigned char tag;
	char* name;
	char* label;
};

struct jfif_marker_range_descr
{
	unsigned char lower_boundary;
	unsigned char upper_boundary;
	char* name;
	char* label;
};


void jfif_parse(unsigned char*, const unsigned long int, struct parser_result*);
bool jfif_skip_chunk(char*, unsigned char**, long int*, unsigned long int*);
char* jfif_tag_name(const enum JFIF_MARKER);
struct jfif_marker_descr* jfif_marker_match(const enum JFIF_MARKER);
struct jfif_marker_range_descr* jfif_marker_range_match(const enum JFIF_MARKER);

#endif
