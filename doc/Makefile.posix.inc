include ../Makefile.posix.inc
include Makefile.defs
include Makefile.inc

T2T = ../devtools/txt2tags.py --no-rc
HTML2PS = ../devtools/html2ps.pl -f html2ps.cfg
PS2PDF = ps2pdf
