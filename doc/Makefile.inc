HTML_DOCS   = DISCLAIMER.html INSTALL.html LICENSE.html README.html TODO.html manpage.html
TXT_DOCS    = DISCLAIMER.txt INSTALL.txt LICENSE.txt README.txt TODO.txt
MAN_DOCS    = manpage.man
PDF_DOCS    = user-manual.pdf
HTML_DIR    = .html
TXT_DIR     = .txt
MAN_DIR     = .man
PDF_DIR     = .pdf