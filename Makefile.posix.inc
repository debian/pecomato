DEL_DIR = rm -Rf
DEL_FILE = rm -f
COPY_FILE = cp -f
CREATE_DIR = mkdir -p
CHECK_EXIST_DIR = test -d
CHECK_NOT_EXIST_DIR = test ! -d
CHECK_EXIST_FILE = test -f
CHECK_NOT_EXIST_FILE = test ! -f
RENAME_FILE = mv -f

PYTHON = python
PERL = perl

TARGZ = GZIP=-9 tar czf
TARBZ2 = BZIP2=-9 tar cjf
TAR = tar cf
GZIP = gzip -9
BZ2 = bzip2 -9
ZIP = zip -9r

SOURCES = *.c *.h
MKFILES = Makefile*.inc Makefile.darwin Makefile.beos Makefile.linux Makefile.unix Makefile.qnx Makefile.win32
RESOURCES = *.ico *.bin *.in
DOC_SOURCES = doc/*.t2t
DOC_MKFILES = doc/Makefile.posix doc/Makefile.win32 doc/Makefile*.inc
DOC_RESOURCES = doc/*.rc doc/*.cfg
TOOLS = devtools/*.py devtools/*.pl devtools/*.sh

CVS2CL = devtools/cvs2cl.pl -bPS --revisions --gmt --fsf --FSF

SYSTBASE = $(shell uname -s | tr "[:upper:]" "[:lower:]")