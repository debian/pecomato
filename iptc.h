#ifndef _iptc_h_
#define _iptc_h_


#include "util.h"

#define IPTC_MARKER 0x1c

enum IPTC_CODE
{
	/* IPTC-NAA IIM 4.1 (few ones from IPTC Core 1.0) / XMP) */
	/* envelope record */
	IPTC_MODEL_VERSION								=0x0100,
	IPTC_DESTINATION								=0x0105,
	IPTC_FILE_FORMAT								=0x0114,
	IPTC_FILE_FORMAT_VERSION						=0x0116,
	IPTC_SERVICE_ID									=0x011e,
	IPTC_ENVELOPE_NUMBER							=0x0128,
	IPTC_PRODUCT_ID									=0x0132,
	IPTC_ENVELOPE_PRIORITY							=0x013c,
	IPTC_DATE_SENT									=0x0146,
	IPTC_TIME_SENT									=0x0150,
	IPTC_CODED_CHARACTER_SET						=0x015a,
	IPTC_UNO										=0x0164,
	/* abstract relationship method identifiers */
	IPTC_ARM_ID										=0x0178,
	IPTC_ARM_VERSION								=0x017a,
	/* Universal Application Record */
	IPTC_APP_RECORD_VERSION						    =0x0200,
	IPTC_APP_OBJECT_TYPE_REFERENCE					=0x0203,
	IPTC_APP_OBJECT_ATTRIBUTE_REFERENCE			    =0x0204,
	IPTC_APP_OBJECT_NAME							=0x0205,
	IPTC_APP_EDIT_STATUS							=0x0207,
	IPTC_APP_EDITORIAL_UPDATE						=0x0208,
	IPTC_APP_URGENCY								=0x020a,
	IPTC_APP_SUBJECT_REFERENCE						=0x020c,
	IPTC_APP_CATEGORY								=0x020f,
	IPTC_APP_CATEGORY_DESCRIPTION					=0x0210,
	IPTC_APP_SUPPLEMENTAL_CATEGORY					=0x0214,
	IPTC_APP_CATEGORY_CODE							=0x0215,
	IPTC_APP_FIXTURE_IDENTIFIER					    =0x0216,
	IPTC_APP_NUM								    =0x0217,
	IPTC_APP_KEYWORDS								=0x0219,
	IPTC_APP_CONTENT_LOCATION_CODE					=0x021a,
	IPTC_APP_CONTENT_LOCATION_NAME					=0x021b,
	IPTC_APP_RELEASE_DATE							=0x021e,
	IPTC_APP_RELEASE_TIME							=0x0223,
	IPTC_APP_EXPIRATION_DATE						=0x0225,
	IPTC_APP_EXPIRATION_TIME						=0x0226,
	IPTC_APP_SPECIAL_INSTRUCTIONS					=0x0228,
	IPTC_APP_ACTION_ADVISED						    =0x022a,
	IPTC_APP_REFERENCE_SERVICE						=0x022d,
	IPTC_APP_REFERENCE_DATE						    =0x022f,
	IPTC_APP_REFERENCE_NUMBER						=0x0232,
	IPTC_APP_DATE_CREATED							=0x0237,
	IPTC_APP_ARCHIVE_DATE							=0x0238,
	IPTC_APP_TIME_CREATED							=0x023c,
	IPTC_APP_DIGITAL_CREATION_DATE					=0x023e,
	IPTC_APP_DIGITAL_CREATION_TIME					=0x023f,
	IPTC_APP_ORIGINATING_PROGRAM					=0x0241,
	IPTC_APP_PROGRAM_VERSION						=0x0246,
	IPTC_APP_OBJECT_CYCLE							=0x024b,
	IPTC_APP_BYLINE								    =0x0250,
	IPTC_APP_BYLINE_TITLE							=0x0255,
	IPTC_APP_TITLE_ORIGINAL							=0x0256,
	IPTC_APP_PRODUCTOR								=0x0257,
	IPTC_APP_ACTOR									=0x0258,
	IPTC_APP_CITY									=0x025a,
	IPTC_APP_SUBLOCATION							=0x025c,
	IPTC_APP_PROVINCE_STATE							=0x025f,
	IPTC_APP_COUNTRY_CODE							=0x0264,
	IPTC_APP_COUNTRY								=0x0265,
	IPTC_APP_ORIGINAL_TRANSMISSION_REFERENCE		=0x0267,
	IPTC_APP_HEADLINE								=0x0269,
	IPTC_APP_CREDIT								    =0x026e,
	IPTC_APP_PHOTOGRAPHER_NUMBER					=0x0270,
	IPTC_APP_SOURCE								    =0x0273,
	IPTC_APP_COPYRIGHT_NOTICE						=0x0274,
	IPTC_APP_CONTACT								=0x0276,
	IPTC_APP_CAPTION_ABSTRACT						=0x0278,
	IPTC_APP_PHOTOGRAPH_NUMBER						=0x0279,
	IPTC_APP_WRITER_EDITOR						    =0x027a,
	IPTC_APP_SERIAL_NUMBER							=0x027b,
	IPTC_APP_RASTERIZED_CAPTION					    =0x027d,
	IPTC_APP_IMAGE_TYPE							    =0x0282,
	IPTC_APP_IMAGE_ORIENTATION						=0x0283,
	IPTC_APP_REFERENCE								=0x0285,
	IPTC_APP_LANGUAGE_IDENTIFIER					=0x0287,
	IPTC_APP_AUDIO_TYPE							    =0x0296,
	IPTC_APP_AUDIO_SAMPLING_RATE					=0x0297,
	IPTC_APP_AUDIO_SAMPLING_RESOLUTION				=0x0298,
	IPTC_APP_AUDIO_DURATION						    =0x0299,
	IPTC_APP_AUDIO_OUTCUE							=0x029a,
	IPTC_APP_OBJECTDATA_PREVIEW_FILE_FORMAT		    =0x02c8,
	IPTC_APP_OBJECTDATA_PREVIEW_FILE_FORMAT_VERSION =0x02c9,
	IPTC_APP_OBJECTDATA_PREVIEW_PREVIEW_DATA		=0x02ca,
	/* newsphoto */
	IPTC_NP_RECORD_VERSION  								=0x0300,
	IPTC_NP_PICTURE_NUMBER  								=0x030a,
	IPTC_NP_PIXELS_PER_LINE 								=0x0314,
	IPTC_NP_NUMBER_OF_LINES 								=0x031e,
	IPTC_NP_PIXEL_SIZE_IN_SCANNING_DIRECTION				=0x0328,
	IPTC_NP_PIXEL_SIZE_PERPENDICULAR_TO_SCANNING_DIRECTION 	=0x0332,
	IPTC_NP_SUPPLEMENTARY_TYPE  							=0x0337,
	IPTC_NP_COLOUR_REPRESENTATION							=0x033c,
	IPTC_NP_INTERCHANGE_COLOUR_SPACE						=0x0340,
	IPTC_NP_COLOUR_SEQUENCE 								=0x0341,
	IPTC_NP_ICC_INPUT_COLOUR_PROFILE						=0x0342,
	IPTC_NP_COLOUR_CALIBRATION_MATRIX_TABLE 				=0x0346,
	IPTC_NP_LOOKUP_TABLE									=0x0350,
	IPTC_NP_NUMBER_OF_INDEX_ENTRIES 						=0x0354,
	IPTC_NP_COLOUR_PALETTE  								=0x0355,
	IPTC_NP_NUMBER_OF_BITS_PER_SAMPLE						=0x0356,
	IPTC_NP_SAMPLING_STRUCTURE  							=0x035a,
	IPTC_NP_SCANNING_DIRECTION  							=0x0364,
	IPTC_NP_IMAGE_ROTATION  								=0x0366,
	IPTC_NP_DATA_COMPRESSION_METHOD 						=0x036e,
	IPTC_NP_QUANTISATION_METHOD 							=0x0378,
	IPTC_NP_END_POINTS  									=0x037d,
	IPTC_NP_EXCURSION_TOLERANCE 							=0x0382,
	IPTC_NP_BITS_PER_COMPONENT  							=0x0387,
	IPTC_NP_MAXIMUM_DENSITY_RANGE							=0x038c,
	IPTC_NP_GAMMA_COMPENSATED_VALUE 						=0x0391,
	/* pre-objectdata descriptor record */
	IPTC_PRODD_SIZE_MODE							=0x070a,
	IPTC_PRODD_MAX_SUBFILE_SIZE						=0x0714,
	IPTC_PRODD_OBJECTDATA_SIZE_ANNOUNCED			=0x075a,
	IPTC_PRODD_MAXIMAL_OBJECTDATA_SIZE				=0x075f,
	/* objectdata descriptor record */
	IPTC_ODD_SUBFILE								=0x080a,
	/* post-objectdata descriptor record */
	IPTC_POODD_CONFIRMED_OBJECT_SIZE				=0x090a,
	/* reserved */
	IPTC_EOT										=0xffff
};

enum IPTC_DATASET_FORMAT
{
	IPTC_DATASET_UNKNOWN,	/* we don't know yet, defaults to binary */
	IPTC_DATASET_NUMERICAL,	/* numerical digits stored as graphical text (allows decimal value checks) */
	IPTC_DATASET_BINARY,	/* dump raw binary bytes (hexadecimal) */
	IPTC_DATASET_TEXT,		/* graphical (printable) characters */
	IPTC_DATASET_SPECIFIC,	/* a kind of encoding format that we KNOW how to decode (for instance,
							   a version number stored in 2 bytes), otherwise it's binary-equivalent */
	IPTC_DATASET_DATE,		/* same as text, but allow date format checks */
	IPTC_DATASET_TIME,		/* same as text, but allow time format checks */
	/* reserved */
	IPTC_DATASET_EOT
};

enum IPTC_DATASET_UNICITY
{
	IPTC_DATASET_REPEATABLE,
	IPTC_DATASET_UNIQUE
};

enum IPTC_DATASET_SIZE
{
	IPTC_DATASET_MAX,
	IPTC_DATASET_FIXED
};

struct iptc_code_descr
{
	enum IPTC_CODE				code;
	enum IPTC_DATASET_FORMAT	format;
	enum IPTC_DATASET_UNICITY	repeatable;
	enum IPTC_DATASET_SIZE		fixed_length;
	long int					length;	/* expected (max or exact) length, 0 to ignore */
	char*						label;
};

enum iptc_filter_element_type
{
	IPTC_FET_VALUE,
	IPTC_FET_RANGE
};

struct iptc_filter_match_data
{
	enum iptc_filter_element_type	type;
	unsigned short int 				code;
	unsigned short int				code2;
};

struct iptc_edit_data
{
	bool				enabled;
	unsigned short int	code;
	size_t				length;
	unsigned char*		data;
};

struct iptc_edit_list_cell
{
	struct iptc_edit_data*		edit_data;
	struct iptc_edit_list_cell*	next;
};

void iptc_parse(unsigned char*, const long int, const bool, struct parser_result*);
void iptc_stats_table_inc(unsigned int, unsigned short int*);
void iptc_stats_table_dec(unsigned int, unsigned short int*);
struct iptc_code_descr* iptc_match_code(const enum IPTC_CODE);
void iptc_dump_value(unsigned short int, struct iptc_code_descr*, unsigned char*, unsigned short int,
			bool dump_length, bool manage_newline);
void iptc_dump_list(void);

void iptc_filter_table_init(void);
void iptc_filter_add_value(unsigned int);
void iptc_filter_add_range(unsigned int, unsigned int);
void iptc_filter_table_reset(void);
void iptc_filter_table_dump(void);
bool iptc_filter_match(unsigned int);
unsigned short int iptc_filters(void);

void iptc_edit_list_init(void);
void iptc_edit_add_edit(unsigned int, size_t, unsigned char*, bool*);
void iptc_edit_list_reset(void);
void iptc_edit_list_dump(void);
unsigned short int iptc_edits(void);
size_t iptc_edit_insert_chunks(bool, unsigned int, unsigned short int*);

#endif
