#include "util.h"

/* Copyright (C) 1991, 1992, 1995, 1996 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the GNU C Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <stdio.h>
#include <sys/types.h>

#if defined _LIBC || defined STDC_HEADERS || defined __STDC__
#include <stdlib.h>
#include <string.h>
#else
char *malloc ();
#endif

char *
strndup (const char *s, size_t n)
{
  size_t len = strnlen (s, n);
  char *ns = malloc (len + 1);

  if (ns == NULL)
    return NULL;

  ns[len] = '\0';
  return (char *) memcpy (ns, s, len);
}

#include <string.h>

/* Find the length of STRING, but scan at most MAXLEN characters.
   If no '\0' terminator is found in that many characters, return MAXLEN.  */

size_t
strnlen (const char *string, size_t maxlen)
{
  const char *end = memchr (string, '\0', maxlen);
  return end ? (size_t) (end - string) : maxlen;
}
