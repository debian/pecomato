#ifndef _config_h_
#define _config_h_


/* define this to show IPTC resource IDs as numerical (for instance: 2:015 instead of 0x020f) */
/* #define IPTC_SHOW_NUMERICAL_ID */

/* define this to always use cached writing (otherwise, cached writing will be used when any
   filter table is used */
/* #define ALWAYS_CACHE_REWRITE */

#endif
