include Makefile.posix.inc
include Makefile.inc

RPMBUILD = rpmbuild -ba
RPM = rpm -ba
RAR = rar a -ow -ol -mdg -m5 -s -r -cfg- -y
7ZIP = 7za a -bd -y

ARCH = $(shell uname -i 2>/dev/null)
ifeq "$(ARCH)" ""
ARCH = i386
endif

SYST = $(SYSTBASE)-glibc-$(shell devtools/glibc-version.sh | sed 's/2\.1\.92/2.2/' | cut -d '.' -f 1-2)-$(ARCH)
