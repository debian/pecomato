# protect '/' chars below

PRODUCT = pecomato
VERSION = 0.0.15
VERSION_RC = 0,0,15,0
DESCRIPTION = A portable metadata processor for picture-embedded contents
AUTHOR_CONTACT = wwp <subscript@free.fr>
HOMEPAGE = http:\/\/mollux.org\/projects\/$(PRODUCT)\/
LICENSE = GPL
CATEGORY = Graphics\/Utilities
